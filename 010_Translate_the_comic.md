# Translate the webcomic

#### Table of Content
1. [License and Code of Conduct](#license)
2. [Get the necessary tools](#tools)
3. [Join us on Framagit](#framagit)
4. [Get the comic pages sources files](#sources)
5. [Translate the SVG comic pages](#editsvg)
6. [Credits and other files](#credits)
7. [Upload your translation](#upload)
8. [F.A.Q.](#FAQ)

![cover image](https://www.peppercarrot.com/0_sources/0ther/website/hi-res/2018-11-22_contribute_02-translation_by-David-Revoy.jpg)
<br/>

**Translating an episode of Pepper&Carrot is fast, fun and easy!** You can do it from any type of computer (Windows, Mac, Linux and more) and using only free/libre tools. In this documentation you'll be able to read all information about managing your own translation of Pepper&Carrot.

<a name="license"></a>
## 1. License and Code of Conduct

By submitting any translation or improvements to the Pepper&Carrot project, you accept that you release your work under the [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/) License (also named CC BY 4.0). 

You also accept the terms of our [Code of Conduct](https://www.peppercarrot.com/xx/documentation/409_Code_of_Conduct.html).


<details>
  <summary>More info</summary>
  <p>
  The license CC BY 4.0 is a license that allows copy and redistribution the material in any medium or format. It includes remix, transform, and build upon the material for any purpose, **even commercially**. The condition: the re-user will need to always provide an appropriate credit.

  You acknowledge that you understand this license and you permit everyone to use your content under this license. You warrant that you have the right to grant this permission and you understand that under the terms of the CC BY 4.0 license you cannot later revoke it.<br/><br/>

  You are of course still free to make derivatives and translations of Pepper&Carrot and distribute them under other licenses, so long as you comply with the applicable licenses. But if you wish to have your translation included on the official website, you must release your work under CC BY 4.0.<br/><br/>

  <em>- Note 1: You can find a similar license on the <a href="https://framagit.org/peppercarrot/webcomics/-/blob/master/CONTRIBUTING.md">CONTRIBUTING file</a> at the root of the repository.</em><br/>
  <em>- Note 2: I disagree with the reusage of my work for NFTs. I explain it <a href="https://www.davidrevoy.com/article864/dream-cats-nfts-don-t-buy-them" target="_blank">on my blog</a>.</em><br/>
  <br/>
  <a href="https://www.peppercarrot.com/xx/documentation/120_License_best_practices.html" class="action">⚖ Attribution best practises</a>

  </p>
</details>

<a name="tools"></a>
## 2. Get the necessary tools

### Inkscape

This is our main tool for editing speech-bubbles and onomatopoeia. It's a free/libre and open-source software available for Windows, macOS and GNU/Linux.

<a href="https://inkscape.org/en/release/" class="action" target="_blank">🏔 Download Inkscape</a>

### Fonts

Pepper&Carrot translations uses only a set of open fonts. Some of them are in-house modified for the project and we distribute them. They are compatible Windows, macOS and GNU/Linux. 

<details>
  <summary>Font-pack for Windows</summary>
  <ol>
    <li><a href="https://www.peppercarrot.com/0_sources/0ther/tools/zip/peppercarrot-fonts-latest.zip">Download peppercarrot-fonts-latest.zip</a></li>  
    <li> Extract peppercarrot-fonts-latest.zip to a temporary folder.</li>  
    <li> Open your Control Panel.</li>  
    <li> Go into Appearance and Personalization.</li>  
    <li> Open Fonts.</li>
    <li> Your folder font will open.</li>
    <li> Drag and drop the Pepper&Carrot fonts into the Fonts window.</li>
    <li> You can now remove your downloaded zip file and the folder you temporary extracted them.</li>
  </ol>
</details>

<details>
  <summary>Font-pack for MacOS</summary>
  <ol>
    <li><a href="https://www.peppercarrot.com/0_sources/0ther/tools/zip/peppercarrot-fonts-latest.zip">Download peppercarrot-fonts-latest.zip</a></li>  
    <li> Extract peppercarrot-fonts-latest.zip to a temporary folder.</li>
    <li> Open Finder.</li>
    <li> Open Applications in the sidebar.</li>
    <li> Select the Font Book application.</li>
    <li> Drag and drop the Pepper&Carrot fonts into Font Book window.</li>
    <li> You can now remove your downloaded zip file and the temporary folder where you extracted them.</li>
  <ol>
  </ol>
</details>

<details>
  <summary>Font-pack for GNU/Linux</summary>
  <ol>
    <li><a href="https://www.peppercarrot.com/0_sources/0ther/tools/zip/peppercarrot-fonts-latest.zip">Download peppercarrot-fonts-latest.zip</a></li>  
    <li> Extract peppercarrot-fonts-latest.zip to a temporary folder.</li>
    <li> Open your file manager.</li>
    <li> Show hidden files.</li>
    <li> Navigate to ~/.local/share/fonts (create the folder if necessary)</li>
    <li> Drag and drop the Pepper&Carrot fonts into ~/.local/share/fonts.</li>
    <li> You can now remove your downloaded zip file and the folder you temporary extracted them.</li><br/>
    Note: For Arch Linux and derivatives, a AUR exists: <a href="https://aur.archlinux.org/packages/peppercarrot-fonts/">https://aur.archlinux.org/packages/peppercarrot-fonts/</a>
    <ol>
  </ol>
</details>

<a name="framagit"></a>
## 3. Join us on Framagit

[Framagit](https://framagit.org) is a tool made for managing collaboratively software and it's hosted by the non-profit organisation [Framasoft](https://framasoft.org/en/). You can find the Pepper&Carrot group on Framagit [here](https://framagit.org/peppercarrot). By creating an account on it, you'll be able to:
- Collaborate on adding, modifying, removing files.
- Discuss with us (using the 'issues' ticket).
- Get notification when someone quote your account, and contact other translators.

This step can be optional but it will be harder to collaborate with you without an account (but you can still send your translations by email).

<a href="https://framagit.org/users/sign_up" class="action" target="_blank">🐧 Get a Framagit account</a>

_Activation issue?... Creating new account on Framagit sometimes takes 48h to be activated and you might not receive any notification about it. It's a known issue: just test to login 48h after account creation and you should be fine._

Once you have your account, tell us a little hello. [Open a new issue ticket](https://framagit.org/peppercarrot/webcomics/-/issues) and present yourself. I'll then grant you permission to upload your files to the source and modify files. You'll join the [long list of Members](https://framagit.org/groups/peppercarrot/-/group_members) as a Pepper&amp;Carrot Developer!

<a name="sources"></a>
## 4. Get the comic pages sources files

Each pages you can see on the comic comes from the fusion of a SVG file (Inkscape) for speech-bubbles and a KRA file (Krita) for artworks. For translation, we only need the SVGs. Multiple methods exists depending of what you want to do:

<details>
  <summary>Get the SVGs of a single episode</summary>
    <ol>
    <li> Go on the page of the episode you want to edit (eg. <a href="https://www.peppercarrot.com/en/webcomic/ep01_Potion-of-Flight.html" target="_blank">Episode 1 in English</a>)</li>  
    <li> Click on the button 'Sources and license' on the top.</li>
    <li> Click on the link inside the gray button 'Text Sources'.</li>
    <li> Save the ZIP archive on your disk.</li>
    <li> Extract the files. Done.</li>
    <ol>
</details>

<details>
  <summary>Get the sources for all episodes</summary>
    <ol>
    <li> Go to the <a href="https://framagit.org/peppercarrot/webcomics" target="_blank">webcomic repository on Framagit</a></li>  
    <li> Near the blue button "Clone", you'll find a button with a Download icon.</li>
    <li> Click on the ZIP format.</li>
    <li> Save the ZIP archive on your disk.</li>
    <li> Extract the files. Done.</li>
    <ol>
</details>

<details>
  <summary>Advanced: clone the Git repository</summary>
    <p>
    On your command line enter:<br/>
    <code>git clone https://framagit.org/peppercarrot/webcomics.git</code><br/>
    <br/>
    It will clone <a href="https://framagit.org/peppercarrot/webcomics" target="_blank">the webcomics repository</a>.
    </p>
</details>

<a name="editsvg"></a>
## 5. Translate the SVG comic pages

![screenshot of duplicating a folder (copy/paste), then renaming it to a fictive language named 'my'](https://www.peppercarrot.com/data/documentation/medias/img/2015-03-01_e_edit-your-inkscape-svg.jpg)

### Avoid duplicate efforts

  Before starting to invest time and effort on your translation, be sure no-one else decided to work on this part too at the same time. It might waste your time! To avoid that:

  <ol>
    <li><a href="https://framagit.org/peppercarrot/webcomics/-/issues">Read the issue tickets on Framagit</a> to check if someone is already working on it.</li>
    <li>If not, it's a good idea to inform <a href="https://framagit.org/peppercarrot/webcomics/-/issues">with a new issue tickets on Framagit</a> that you are on it.</li>
  </ol>

### Editing the comic 

Episode have directories formated like ``ep01_Potion-of-Flight``, inside you'll find a subdirectory named ``lang``. This directory contains all the translation already made for this episode. Open a language sub-directory and you'll see the SVGs pages ready to edit with Inkscape. 

<details>
  <summary>How to add a new language?</summary>
  <br/>
  To create a new language, just copy/paste an existing language directory (the English directory <code>en</code> is the most used starting point, but feel free to start by the language of your choice) and then rename the pasted directory into your target language.<br/><br/>

  For the choice of the two small-case letters of your lang, try to use <a href="https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes">the ISO 639-1</a> language codes when it's possible, but if it doesn't exist, we have the flexibility to invent one. That's why we call our two-letter language code <em>the Pepper&Carrot lang ID</em>. <a href="https://framagit.org/peppercarrot/webcomics/-/issues">Create a new issue tickets on Framagit</a> to contact us if you have doubt about this two letter choice.
  <br/><br/>
  Later in the process, when your translation will be done, we'll need information about your new language. You'll have to edit the <code>langs.json</code> file at the root of the comic to enter this information. A dedicated page exist about this file; but don't worry too much if you can't edit it alone. We can help you later to do so.
  <br/>
</details>

Once in Inkscape, double clicking a text box will edit it. It's the same process for onomatopoeia. Speech-bubble and their tails are object you can resize, scale, rotate and move around. Resize them so they wrap better around your text. Feel free to be the art director of your translation: resize stuff and arrange the layout to your taste. Don't forget to fill in the credits for your translation on the credit page (the last page).

If you are interested in advanced Inkscape techniques and a FAQ about translation, read the Translation Tips page:

<a href="https://www.peppercarrot.com/xx/documentation/060_Translation_Tips.html" class="action" target="_blank">💬 Translation Tips page</a>

_Note: If Inkscape prompts you with a dialog at start-up "Convert Legacy Inkscape file" looking like [that](https://www.peppercarrot.com/data/documentation/medias/img/convert-legacy-inkscape-file.jpg), just keep the default option "This file contains digital artwork for screen display. (Choose if unsure)" and then press the OK button._

<a name="credits"></a>
## 6. Credits and other files

### info.json file

**This file is important**: it will manage your credit. You can simply edit it with a text editor. If you have doubt how to format it, just look how other language directory wrote it or check the full documentation:

<a href="https://www.peppercarrot.com/xx/documentation/081_info.json.html" class="action" target="_blank">🗒️ info.json full documentation</a>

### References file

**This file is optional**: they help you to keep notes on how you translate characters, place and creature. It also helps to auto-generate transcripts. 

If you have downloaded all the repository, you'll find the references of your language −a Po file named with the letter of your language− under the directory named ``1_translation-names-references``.

<details>
  <summary>Edit the PO reference files</summary>
    <p>
    You can edit Po files with a simple text editor or a softwares for Po files. <a href="https://poedit.net/">Poedit</a> is a good Free/Libre and open-source software for that. The files are composed mainly of three type of entries. Here is a sample item from <code>fr.po</code>: <br/>
    <br/>
    <code>#. A student witch of Hippiah and also an elf, with red hair.</code><br/>
    <code>msgid "Cinnamon"</code><br/>
    <code>msgstr "Cannelle"</code><br/>
    <br/>
    - First line (always starts by <code>#.</code>) is a comment to help you to get context<br/>
    - The a line starting by <code>msgid</code> with the English reference (to leave as it is).<br/>
    - Finally, the line starting by <code>msgstr</code> is the most important and the one to translate. (Note: keep the word wrapped between <code>"</code>)<br/>

    </p>
</details>

### Transcript

**This file is optional**: you don't need to translate it and you can delete it. Transcript are auto-generated later from the SVGs you translated. If you want to know how, check the transcript documentation:

<a href="https://www.peppercarrot.com/xx/documentation/062_Transcripts.html" class="action" target="_blank">🗒️ Transcript full documentation</a>

<a name="upload"></a>
## 7. Upload your translation

When your translation is complete and looks good, it is time to send it to the Pepper&Carrot official website. You have mainly three options to upload your translation:


<details>
  <summary>Post your translation on Framagit</summary>
    <ol>
    <em>Note: for this steps, you'll need a Framagit account.</em><br>
    <br>
      <b>Create a Branch</b>
    <li> Open <a href="https://framagit.org/peppercarrot/webcomics/-/branches">the Branches page</a> of the webcomic repository on Framagit</a></li>  
    <li> Click the top-right colored button "New branch".</li>
    <li> Name your new branch as you want (eg. a short and informative "ep26-fr" for episode 26 in French) </li>
    <li> Leave other option unchanged and click the button "Create branch" on bottom-left to complete the creation.</li>
    <br>
      <b>Add your file to the branch</b><br>
    <li> Right after the creation, the page probably reloaded directly inside your Branch. If you are not sure, you can still access your branch within the <a href="https://framagit.org/peppercarrot/webcomics/-/branches">the Branches page</a> and click on its title to display the repository of your branch.</li>
    <li> Then, in your branch, click the button 'Web IDE' on the header of the repository.</li>
    <li> The IDE will launch and a file listing will appear on the left of your monitor. With this menu, you can control the files: you can expand directories, use the three dots menu (appearing while having the cursor over a directory) to create 'New directory' and 'Upload files'. You can then create the directory of your lang and upload all your SVG at once this way.</li>
    <li> When it's done and you are happy with the modification, finalize your action with the blue 'Commit...' button on the bottom left of the page.</li>
    <li> You can inform a commit message (eg. Translation for episode 26 in French), leave other option unchanged (ensure 'Start a new merge request' is checked), and press the blue 'Commit' button.</li>
    <br>
      <b>Create the Merge Request</b>
    <li> After you commited your changes (if you checked 'create a merge request') you should be now on the 'New merge Request' page..</li>
    <li> Add a simple title, a description and press the blue 'Create merge request' button on the bottom.<br/><br/>
    </li>

    <ol>
</details>

<details>
  <summary>Advanced method using only Git</summary>
    <p>
    <em>Note: for pushing commits, you'll need a Framagit account.<br/>
    Also: Master branch is protected.</em><br>
    If you clicked on this item, you probably already know the workflow:<br/>
    <br/>
    <b>Create a Branch</b><br/>
    <code>git checkout -b YOURBRANCHNAME</code><br/>
    <br/>
    <b>Add your files, then commit</b><br/>
    <code>git add -A</code><br/>
    <code>git commit</code><br/>
    <br/>
    <b>Create the Merge Request</b><br/>
    <code>git push -u origin YOURBRANCHNAME</code><br/>
    A message will appear after pushing; mentioning an URL to invite you to create a Merge Request.
    <br/>
    </p>
</details>

<details>
  <summary>Send the translation by email</summary>
    <ol>
    <li> Please ZIP back your SVG translated files and your info.json file.</li>  
    <li> Attach it in a new email to <a href="mailto:info@davidrevoy.com">info@davidrevoy.com</a>. Done.<br>
    <br>
    <em> Note: Please inform me about the episode and the target language in the email, so I know where to commit the files. </em>
    </li>
    <ol>
</details>

### Done! **Congratulation!**

You sent a Pepper&Carrot translation! 🎉  
Thank you for your contribution! 

![Done](https://www.peppercarrot.com/data/documentation/medias/img/done.png "Pepper blowing her wand") 

<a name="FAQ"></a>
## 8. F.A.Q.

**Q:** **Where can I subscribe to receive notifications about new episodes ready to be translated?**  
A: Each time a new episode is ready for translation, I notify all project member on Framagit. [Create a Framagit account](https://www.peppercarrot.com/xx/documentation/010_Translate_the_comic.html#framagit) and become a member of our group to receive this notification.

**Q:** **Is it possible to change the characters' names for a translation?**  
A: Sure, you can. For the French translation, I kept the english 'Carrot' because the French name "Carotte" is feminine. I kept 'Pepper' and didn't use the translation 'Poivre' or 'Poivron' because it sounds like an insult French-speakers may throw at a drunkard, 'Poivrot!'. 

**Q: When I open a SVG, Inkscape prompt me with a dialog window at start-up "Convert Legacy Inkscape file", what should I do?**  
A: The one looking like [that](https://www.peppercarrot.com/data/documentation/medias/img/convert-legacy-inkscape-file.jpg)? Just keep the default option "This file contains digital artwork for screen display. (Choose if unsure)" and then press the OK button.

**Q: A sentence doesn't sound good in my language, can I rewrite it?**  
A: That's ok. The most important is to keep the semantic, the feeling and the information of the story. Adding style and changing the expression of your target language is natural.

**Q: Do I need to translate all the episodes to get published on the website?**  
A: No. Feel free to contribute within your possibilities: you can translate just one episode then try one another later. Take your time. The website is designed to fall back to English if there's no translation available for a given episode. So, you can send episode 01, then one month later send episode 03, 04, 05 in a row, then stop here if you want... or then send all the other episodes. Every contribution is welcome; thank you for your effort and contributions!

**Q: Can I translate fictive language, as Klingon, Dothraki, Quenya, Sindarin?**  
A: Yes and No. Yes because you are free to translate the SVG and publish them on your blog/forum as a cross-over with a fair-use "fan-art" of Copyrighted material. 
No, if you plan to get this files accepted into the main repository of Pepper&Carrot because it is not Creative Commons compatible. You can read more about it on [this thread](https://framagit.org/peppercarrot/webcomics/issues/60).

**Q: A SVG is missing in the translation folder; at episode four: E04P05.svg!**  
A: The file is indeed missing on purpose: in the comic this panel is a GIF animation and can't be translated. I had to remove the SVG for this page so the renderfarm ignore computing a translation for this specific case. A special set of rules are applied each time a GIF animation appears in the story. That's why it is a rare situation.

