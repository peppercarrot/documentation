# Fan Comic Submission Guidelines

Welcome to the Pepper&Carrot fan comic submission guidelines. We're excited to have you share your creations with our community. To make the process as smooth as possible, we've put together this guide to help you format your comic for our gallery here: https://www.peppercarrot.com/en/fan-art/fan-comics.html

### The Thumbnail

The thumbnail is the first thing our visitors will see, so make it count. Here's what you need to know:

* The directory name should include the title of your series and your name, separated by an underscore `_by_` pattern. For example: `Your-Title-Of-Your-Choice_by_Your-Name`
* The thumbnail image should be a minimum of 720x720px, in JPG format, with a quality of 92. Name it `00_cover.jpg`.

### The Index

When a visitor clicks on your thumbnail, they'll be taken to the index page, which lists all the comics in your series. Here's how to set it up:

* The introduction text is managed by a Markdown file named `<lang>_info.md`, where `<lang>` is the language code (e.g., `en` for English, `fr` for French, etc.). You can write a pragraph or two about your creation, giving some context always is appreciated.
* The links to your comic's sources and translation documentation are managed by a JSON file named `links.json`. This file should have two optional keys: `git-repository` and `translation-documentation`. Here's an example:
```json
{
  "git-repository": "https://framagit.org/peppercarrot/derivations/peppercarrot_mini/",
  "translation-documentation": "https://framagit.org/peppercarrot/derivations/peppercarrot_mini/blob/master/CONTRIBUTING.md"
}
```
* Finally, you'll need to create an `info.json` file with a single option: the background color for your comic page, in hex color format for CSS. For example:
```json
{
  "background-color": "#364c1df"
}
```
(see the green background of [The Storm](https://www.peppercarrot.com/en/fan-art/fan-comics__The-Storm_by_Jana-Kus-and-Juan-Jose-Segura~~en_ZSTORM_E01P01_by-Jana-Kus-and-Juan-Jose-Segura.html) to see it in action)

### The Comic Pages

Now it's time to talk about the comic pages themselves. Here's what you need to know:

The first page of your comic will be used as the thumbnail in the index. Each comic page should be named in the following format: 

`<lang>_CODENAMEOFYOURSERIES_EXXPXX_By-Author.jpg`


* `<lang>` is the language code (e.g., `en` for English, `fr` for French, etc.).
* `CODENAMEOFYOURSERIES` is the codename of your series, in capitals (e.g., `PEPPERCARROT`).
* `EXXPXX` is the episode and page number (e.g., `E03P05`, `E01P02`).
* `by-Author` is the author's name.
* If you want to provide a custom episode cover, you can create a square JPG file named `<lang>_CODENAMEOFYOURSERIES_EXX.jpg`. If not, the website will auto-generate a thumbnail.

### File Format

For the website, we recommend using a `1200px` wide JPG file with a compression of 92% for the final exported version. Your source file (eg. Krita source file) should be twice this resolution (eg 2400px large) minimum or x3. You can keep this source files as big and lossless as you want and it's not an obligation to share them. But if you do, prefer PNG (if flat), PSD, KRA or ORA format and sRGB/8bit.

### That's It!

We hope this guide has been helpful. If you have any questions or need further clarification, don't hesitate to ask. 

You can also check out other already published files on [https://www.peppercarrot.com/0_sources/0ther/community/](https://www.peppercarrot.com/0_sources/0ther/community/). Happy creating!
